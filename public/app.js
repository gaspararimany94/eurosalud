$(document).ready(function () {
    $('.sidenav').sidenav();
    $('.dropdown-trigger').dropdown();
    $('.collapsible').collapsible();
    $('.parallax').parallax();
    $('.carousel').carousel();
    var instance = M.Sidenav.getInstance($('#slide-out'));
    $('#SideNav').on('click', function () {
        instance.open();
    });
    new WOW().init();
    $('#contacto').webuiPopover({ url: '#modalContacto' });
});

$(window).on('load', function () {
    setTimeout(function () {
        $('#preLoader').hide();
    }, 2000)
});

// $(window).load(function () {
//    $('#preLoader').hide();
//    alert("window is loaded");
// });


// window.onload = function(){
//     $('#preLoader').hide();
// }


