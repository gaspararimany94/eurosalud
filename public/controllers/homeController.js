app.controller('homeController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $('.parallax').parallax();
    $('.modal').modal();
    $('.slider').slider();
    $('.collapsible').collapsible();
    $('.carousel').carousel();
    $('.tabs').tabs();

    //$rootScope.showLoader = false;
}])