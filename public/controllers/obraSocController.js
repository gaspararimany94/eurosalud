app.controller('obraSocController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $('.modal').modal();
    $('.slider').slider({
        height:500
    });
    $('.collapsible').collapsible();
    $('select').formSelect();

    //descomentar en caso de querer remover pulse on click
    // $('.pulse').on("click", function(){
    //     $(this).removeClass('pulse');
    // });
}])