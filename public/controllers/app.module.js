var app = angular.module('euroApp', ['ui.router'])

app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {


    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise("/");

    var homeState = {
        name: 'home',
        url: '/home',
        templateUrl: './views/home.html',
        controller: 'homeController'
    }

    var indexState = {
        name: 'index',
        url: '/',
        templateUrl: './views/home2.html',
        controller: 'homeController'
    }

    var obraSocialState = {
        name: 'obraSocial',
        url: '/eurosalud',
        templateUrl: './views/Mutual.html',
        controller:'obraSocController'
    }

    var informadorState = {
        name:'informador',
        url:'/informador',
        templateUrl:'./views/informador.html',
        controller: 'informadorController'
    }

    $stateProvider.state(indexState).state(homeState).state(obraSocialState).state(informadorState);
});

app.run(['$rootScope', '$transitions', '$state', '$location', '$window', function ($rootScope, $transitions, $state, $location, $window) {
    $transitions.onExit({}, function ($state, $transitions, $location) {
        // $rootScope.showLoader = true;
        $('.sidenav').sidenav('close');
    });
}]);