let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

var port = process.env.PORT || 8000

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static('public'));

app.use(function (req, res, next) {
    var url = path.resolve(__dirname + '/public/index.html');
    res.sendFile(url, null, function (err) {
        if (err) res.status(500).send(err);
        else res.status(200).end();
    });

    // var err = new Error('Not Found');
    // err.status = 404;
    // next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500).send({ Mensaje: err.message });
    //res.render('error');
});

app.listen(port, function (err) {
    console.log(`hosteado en ${port}`);
})